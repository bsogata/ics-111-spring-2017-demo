package edu.hawaii.ics111.demo;

import java.util.Scanner;

/**
 * A simple calculator that returns the result of a user-provided calculation.
 * 
 * Due to budget constraints, this calculator only handles a single operation.
 *
 * @author Branden Ogata
 */
public class Calculator {

  /**
   * Prompts the user to input a mathematical equation, then prints the result of that equation.
   * @param args    The String[] containing command line arguments; not used.
   */
  public static void main(String[] args) {
		System.out.print("Enter a math expression: ");
    Scanner input = new Scanner(System.in);
    
    String equation = input.nextLine();
    
    try {
      if (equation.contains("+")) {
        int operandIndex = equation.indexOf("+");
        double firstOperand = Double.parseDouble(equation.substring(0, operandIndex).trim());
        double secondOperand = Double.parseDouble(equation.substring(operandIndex + 1).trim());
        System.out.println(add(firstOperand, secondOperand));
      }
      else if (equation.contains("-")) {
        int operandIndex = equation.indexOf("-");
        double firstOperand = Double.parseDouble(equation.substring(0, operandIndex).trim());
        double secondOperand = Double.parseDouble(equation.substring(operandIndex + 1).trim());
        System.out.println(subtract(firstOperand, secondOperand));
      }
      else if (equation.contains("*")) {
        int operandIndex = equation.indexOf("*");
        double firstOperand = Double.parseDouble(equation.substring(0, operandIndex).trim());
        double secondOperand = Double.parseDouble(equation.substring(operandIndex + 1).trim());
        System.out.println(multiply(firstOperand, secondOperand));
      }
      else if (equation.contains("/")) {
        int operandIndex = equation.indexOf("/");
        double firstOperand = Double.parseDouble(equation.substring(0, operandIndex).trim());
        double secondOperand = Double.parseDouble(equation.substring(operandIndex + 1).trim());
        System.out.println(divide(firstOperand, secondOperand));
      }
    }
    catch (NumberFormatException e) {
      System.err.println(e.getMessage());
    }
    
    input.close();
  }
  
  /**
   * Returns the sum of the two parameters.
   * 
   * @param first     The double equal to the first operand of the addition.
   * @param second    The double equal to the second operand of the addition.
   * 
   * @return The sum of the two parameters.
   */
  public static double add(double first, double second) {
    double sum = first + second;
    return sum;
  }

  /**
   * Returns the difference of the two parameters.
   * 
   * @param first     The double equal to the first operand of the subtraction.
   * @param second    The double equal to the second operand of the subtraction
   * @return The difference of the two parameters.
   */
  public static double subtract(double first, double second) {
    return first - second;
  }
  
  /**
   * Returns the product of the two parameters.
   * 
   * @param first     The double equal to the first operand of the multiplication.
   * @param second    The double equal to the second operand of the multiplication.
   * 
   * @return The product of the two parameters.
   */
  public static double multiply(double first, double second) {
    return 0;
  }

  /**
   * Returns the quotient of the two parameters.
   *
   * @param first     The double equal to the dividend of the division.
   * @param second    The double equal to the divisor of the division.
   * @return The quotient of the two parameters.
   */
  public static double divide(double first, double second) {
    return 0;
  }
}