package edu.hawaii.ics111.demo;

import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the Calculator class.
 *
 * @author Branden Ogata
 */
public class TestCalculator {

  /**
   * Sets up the Calculator tests; not used.
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }


  /**
   * Cleans up after the Calculator tests; not used.
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Tests the add method.
   */
  @Test
  public void testAdd() {
    assertEquals("2 + 2 == 4", 4, Calculator.add(2, 2), 0.0001);
    assertEquals("0 + 2 == 2", 2, Calculator.add(0, 2), 0.0001);
  }

}
